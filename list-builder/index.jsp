<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>List Builder</title>
    <link rel="stylesheet" type="text/css" href="./css/ListBuilder.css" />
    <link rel="stylesheet" type="text/css" href="../lib/jquery-ui-1.11.4.custom/jquery-ui.min.css" />
</head>
<body>
    <h1>List Builder</h1>
    <div id="list-1" class="list-builder-container"></div>
    <div id="list-2" class="list-builder-container"></div>
    
    <h2>Overview</h2>

    <h2>Use</h2>

    <p>ListBuilder is built into a jQuery-selected div.</p>

    <!-- icons and template  -->
    <div style="display:none">
        <%@include file="../lib/icons.svg"%>
        <%@include file="./templates/listBuilderTemplate.html"%>
        <%@include file="./templates/filterSettings.html"%>
    </div>
    <script src="../lib/jquery-2.1.4.min.js"></script>
    <script src="../lib/jquery.csv-0.71.min.js"></script>
    <script src="../lib/jquery-ui-1.11.4.custom/jquery-ui.min.js"></script>
    <script src="./js/ListBuilder.js"></script>
    <script>
        var data1 = [
            {name:"the"},
            {name:"quick"},
            {name:"brown"},
            {name:"fox"},
            {name:"jumped"},
            {name:"over"},
            {name:"lazy"},
            {name:"sleeping"},
            {name:"dogs"}
        ];

        $("#list-1").ListBuilder({data:data1});

        $.get("Leinhardt.csv", function(csv) {
            var objs = $.csv.toObjects(csv); 
            objs.forEach(function (d) {
                d.name = d.country.replace(/\./g," ");
                delete d.country;
            });
            $("#list-2").ListBuilder({data:objs});
        });

        $.get("Leinhardt.csv", function(csv) {
            var objs = $.csv.toObjects(csv);
            var data = [];
            objs.forEach(function(d) {
                if (d.region === "Africa") {
                    d.name = d.country.replace(/\./g," ");
                    delete d.country;
                    data.push(d);
                }
            });
            $("#list-2").ListBuilder("setData", data);
        });
    </script>
</body>
</html> 
