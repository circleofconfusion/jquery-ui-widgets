/**
 * @author  Shane Knudsen
 * @file    ListBuilder.js
 *
 * Creates a list builder widget inside a designated div.
 */
;(function ($, window, document) {

    $.widget("custom.ListBuilder", {
        options: {
            data:[],
            listFormat: function(dataItem) {
                var $li = $('<li></li>');
                $li.text(dataItem.name);
                $li.data(dataItem);
                return $li;
            }
        },

        _create: function() {

            // copy the template and place it in the selected container
            $("#lb-wrapper")
                .clone()
                .removeAttr("id")
                .addClass("lb-wrapper")
                .appendTo(this.element);


            // create a handle for the filter field, and set an event handler
            this.$filterInput = this.element.find(".lb-filter-input");
            this.$filterInput.on("keyup", $.proxy(this._filterAvailable, this));

            // create a handle for the filter settings button and set an event handler
            this.$filterSettings = this.element.find(".lb-filter-settings");
            this.$filterSettings.on("click", $.proxy(this._showFilterSettings, this));

            // create handles for available and selected lists
            this.$available = this.element.find(".lb-available-list");
            this.$selected = this.element.find(".lb-selected-list");

            // set events on some components
            this.$toSelected = this.element.find(".lb-move-button-to-selected");
            this.$toAvailable = this.element.find(".lb-move-button-to-available");
            this.$toSelected.on("click", $.proxy(this._copyToSelected, this));
            this.$toAvailable.on("click", $.proxy(this._removeFromSelected, this));

            // place the initial contents
            this._populateAvailable();
            this._setupFiltering();
        },

        /**
         * Populates the list with data items.
         */
        _populateAvailable: function() {
            var self = this;
            // clear out the list
            this.$available.html("");
            // loop through this.options.data and make a list item for each
            for (var i in this.options.data) {
                var $li = this.options.listFormat(this.options.data[i]);
                $li.on("click", function(ev) { if (!$(ev.target).hasClass("copied")) $(ev.target).toggleClass("selected"); });
                $li.on("dblclick", function(ev) { 
                    if (!$(ev.target).hasClass("copied")) { 
                        $(ev.target).toggleClass("selected"); 
                        self._copyToSelected();
                    }
                });
                $li.appendTo(this.$available);
            }
        },

        /**
         * Sets up filtering of available data
         */
        _setupFiltering: function() {
            var self = this;
            this.$filterDialog = $("#lb-filter-dialog").clone();
            this.$filterDialog.removeAttr("id").addClass("lb-filter-dialog");
            this.$filterDialog.dialog({
                title:"Filter Search On",
                autoOpen: false,
                buttons: [
                    {
                        text:"Set",
                        click: function() {
                            self._filterOn = [];
                            self.$filterDialog.find("input:checked").each(function() {
                                self._filterOn.push($(this).attr("data-key"));
                            });
                            $(this).dialog("close");
                            self._filterAvailable();
                        }
                    },
                    {
                        text:"Cancel",
                        click: function() {
                            $(this).dialog("close");
                        }
                    }
                ]
            });

            this.$filterList = this.$filterDialog.find("ul");
            var fKeys = Object.keys(this.options.data[0]);
            for (var i in fKeys) {    
                var $li = $('<li><label><input type="checkbox" data-key=' + fKeys[i] + ' checked />' + fKeys[i] + '</label></li>');
                $li.appendTo(this.$filterList);
            }
            this._filterOn = fKeys;

        },

        /**
         * Filters the list items based on the contents of this.$filterInput.
         */
        _filterAvailable: function() {
            var self = this;
            var txt = this.$filterInput.val().toLowerCase();
            var $items = this.$available.find("li");
            $items.removeClass("non-matching");
            if (txt.trim() == "") 
                return;

            // loop through each item and find out if it matches the filter
            $items.each(function(i, elem) {
                var $elem = $(elem);
                var edata = $elem.data();
                var nonMatching = true;
                for (var i in self._filterOn) {
                    if (edata.hasOwnProperty(self._filterOn[i]) && edata[self._filterOn[i]].toLowerCase().indexOf(txt) != -1) {
                        nonMatching = false;
                        break;
                    }
                }
                if (nonMatching)
                    $elem.removeClass("selected").addClass("non-matching");
            });

        },

        /**
         * Shows the filter settings dialog
         */
        _showFilterSettings: function() {
            this.$filterDialog.dialog("open");
        },
       
        /**  
         * Copies every list item with the selected CSS class to
         * the selected list.
         */
        _copyToSelected: function() {
            var self = this;
            var $selected = this.$selected;
            this.$available.find("li.selected").removeClass("selected").each(function(i, elem) {
                var $li = $(elem);
                var $newLi = $li.clone();
                $li.addClass("copied");
                $newLi.data($li.data());
                $newLi.appendTo($selected);
                $newLi.on("click", function(ev) { $(ev.target).toggleClass("selected"); });
                $newLi.on("dblclick", function(ev) { 
                    $(ev.target).toggleClass("selected");
                    self._removeFromSelected(); 
                });
            });
        },

        /**
         * Removes item(s) from the selected list and removes
         * the 'copied' class from their counterparts in the available list.
         */
        _removeFromSelected: function() {
            var self = this;
            var $available = this.$available.find("li");
            this.$selected.find("li.selected").each(function (i, aelem) {
                var $li = $(aelem);
                var adata = $li.data();
                $available.each(function(j, belem) {
                    var $belem = $(belem);
                    var bdata = $belem.data();
                    if (self._equals(adata, bdata))
                        $belem.removeClass("copied");
                });
                $li.remove();
            });
        },

        /**
         * Deep compares two objects for equality.
         * Does not compare functions.
         */
        _equals: function(a,b) {
            var aKeys = Object.keys(a);
            var bKeys = Object.keys(b);
            // check to see we have the same number of keys
            if (aKeys.length != bKeys.length)
                return false;
            
            // loop through the keys comparing values
            for (i in aKeys) {
                if (typeof aKeys[i] === 'Array' || typeof aKeys[i] === 'Object')
                    if (!this._equals(a[aKeys[i]], b[bKeys[i]])) {
                        return false;
                        continue;
                    }
                if (a[aKeys[i]] !== b[bKeys[i]])
                    return false;
            }

            return true;
        },

        /**
         * Gets an array of selected data from the selected list.
         */
        getSelected: function() {
            var data = [];
            this.$selected.each(function(i, elem) {
                data.push($(elem).data);
            });
            return data;
        },

        /**
         * Sets this.options.data and updates the list of available items
         */
        setData: function(data) {
            this.options.data = data;
            this._populateAvailable();
            this._setupFiltering()
        }

    });

})(jQuery, window, document);
