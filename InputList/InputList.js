/*
 * InputList
 * @author  Shane Knudsen <sknudsen@hammers.com>
 *
 * A jQuery plugin that takes an unordered list, inserts an input element into
 * it, and allows for adding and editing the elements within.
 *
 * Dependencies:
 *      jquery.js
 *      jquery-ui.js
 *      underscore.js
 */

/* TODO:
 *      Allow dragging of elements into various places in the list.
 *      
 * BUGS:
 *      Regression:the arrow keys no longer create an intermediate input element 
 *          when arrowing, and instead go directly to the prev/next element.
 */
;(function($, window, document, undefined) {

    $.widget("custom.InputList", {
        options: {

            /**
             * Default function to organize data as an array of objects with 
             * only name attributes.
             * Assumes that data was passed in as a simple list.
             * Pass a dataFunction in when instantiating to override.
             */
            dataFunction: function(data) {
                var arr = [];
                data.forEach(function(ce) {
                    arr.push({name:ce});
                });
                return arr;
            },

            autocompleteData: [],

            /**
             * Default function to format the dropdown list items' innerHTML.
             * Generates HTML: xxx<strong>mm</strong>xxxxx
             * @param elem  A data object in this.autocompleteData that will have
             *              extra matchIndex and matchLength properties.
             */
            dropdownLiFormat: function(elem) {
                // swap out the match for a <strong>match</strong>
                var elemSubstr = elem.name.substr(elem.matchIndex, elem.matchLength);
                return elem.name.replace(elemSubstr,"<strong>" + elemSubstr + "</strong>");
            },
        
            /**
             * If undefined, all data types are accepted.
             */
            dataTypesAccepted: undefined,

            // Existing elements
            existingElements: undefined
        },

        _create: function() {
            // set the basic elements
            this.$list = this.element;
            this._createInputItem(); 
            this.$list.append(this.$inputItem);
            this.$list.wrap('<div class="input-list-wrapper"></div>');
            this.$wrapper = this.$list.parent();
            this.autocompleteData = this.options.dataFunction(this.options.autocompleteData);
            this._createDropdown();

            // set event handlers for the list
            this.$list.on("click", $.proxy(this._listHandle.click, this));

            // populate with existing items, if any
            for (var elem in this.options.existingElements) {
                this.appendItem(this.options.existingElements[elem]);
            }
        },

        /**
         * Returns an object containing an array of all the list item data in the list, 
         * and a space-separated string of all the item.name value valuess.
         */
        getValues: function () {
            var vals = {
                    list: []
                };
            this.$list.find("li.item").each(function () {
                vals.list.push($(this).data());
            });

            vals.string = vals.list.map(function(elem) { return elem.name }).join(" ");

            return vals;
        },

        /**
         * Sets the values of the list.
         */
        setItems: function(vals) {
            
        },

        /**
         * Creates a new list item but doesn't place it anywhere
         * @param data  JSON data in the form being used by this plugin instance.
         * @return      The newly created jQuery wrapped element.
         */
        _createItem: function(data) {
            // define a new item
            var $newItem = $('<li class="item"></li>');
            $newItem.data(data);
            $newItem.text($newItem.data("name"));
            // add event handlers
            $newItem.on("mousedown", $.proxy(this._listItemHandle.mousedown, this));
            $newItem.on("click", $.proxy(this._listItemHandle.click, this));
            
            return $newItem;
        },

        /**
         * Adds a list item to the list.
         */
        appendItem: function(data) {
            var $newItem = this._createItem(data);

            // place element in list
            var $last = this.$list.find("li:last-child");
            if ($last.hasClass("input-item")) {
                $last.before($newItem);
            }
            else if ($last.hasClass("item")) {
                $last.after($newItem);
            }
        },

        /**
         * Creates the autocomplete dropdown.
         */
        _createDropdown: function() {
            this.$dropdown = $('<ul class="input-list-dropdown"></ul>');
            this.$wrapper.append(this.$dropdown);
            this.$dropdown.hide();
        },

        /**
         * Creates a new input list item and assigns event handlers.
         */
        _createInputItem: function() {
            this.$inputItem = $('<li class="input-item"></li>');
            this.$inputField = $('<input class="input-field" style="min-width:0.1em" type="text" value="" />');
            this.$inputItem.append(this.$inputField);
            this.$inputField.on("focus", $.proxy(this._inputHandle.focus, this));
            this.$inputField.on("blur", $.proxy(this._inputHandle.blur, this));
            this.$inputField.on("keydown", $.proxy(this._inputHandle.keydown, this));
            this.$inputField.on("keyup", $.proxy(this._inputHandle.keyup, this));
            this.$inputField.on("click", $.proxy(this._inputHandle.click, this));
            // Status information about the input field.
            this.$inputField._status = {
                //editing: false,
                cursorAtBeg: false, // whether or not the left arrow key was pressed when cursor was at the beginning of this.$inputField
                cursorAtEnd: false  // whether or not the right arrow key was pressed when cursor was at the end of this.$inputField
            };
        },

        /* The next two functions do the same thing other than where they
         * place the new element.
         * It's handy to differentiate when using arrow keys to create
         * new elements.
         */

        /**
         * Creates a new list item based on the $input element's value,
         * and places it before the $inputItem.
         * @param   data    The data to be attached to the item.
         */
        _createListItemBefore: function(data) {
            var $newItem = this._createItem(data);

            this._resetDropdown();

            this.$inputItem.before($newItem);
            this.$inputItem.removeData();
            this._setInputVal("");

            this.$inputField.focus();
            // The input will be set to some odd width. Reset it.
            this.$inputField.css("width","");
        },

        /**
         * Creates a new list item based on the $input element's value,
         * and places it after the $inputItem.
         * @param   data    The data to be attached to the item.
         */
        _createListItemAfter: function(data) {
            var $newItem = this._createItem(data);
            
            this._resetDropdown();

            this.$inputItem.after($newItem);
            this.$inputItem.removeData();
            this._setInputVal("");

            this.$inputField.focus();
            // The input will be set to some odd witdh. Reset it.
            this.$inputField.css("width","");
        },

        /**
         * Enables editing of a selected list item
         * by swapping it out for the designated input list item
         * and setting the input to the existing value.
         * @param   The HTML (not jQuery) node to edit.
         */
        _editListItem: function(elem) {
            // see if we were editing something else first, so we don't lose it
            if (this.$inputField.val().trim().length > 0)
                this._createListItemBefore(this._getDropdownData());
            
            var $elem = $(elem);
            var elemData = $elem.data();

            $elem.after(this.$inputItem);
            $elem.remove();
            
            this.$inputItem.data(elemData);
            this.$inputField.val(elemData.name).focus().select();
            this._setInputWidth();

            this._updateDropdown(name);
        },

        /**
         * Clears out the entire list
         */
        clearList: function() {
            this.$list.find(".item").remove();
        },

        /**
         * Namespace containing functions for handling events on the list itself.
         */
        _listHandle: {
        
            click: function(e) {
                e.stopPropagation();
                var eX = e.pageX;
                var eY = e.pageY;
                var $listItems = this.$list.find("li.item");
                
                // for an empty list, just focus the input and return
                if ($listItems.length == 0) {
                    this.$inputField.focus();
                    return;
                }

                // create a reference to the plugin for use inside the function below
                var plugin = this;

                $listItems.each(function(index, element) {
                    var left = $(this).offset().left;
                    var top = $(this).offset().top;
                    var right = left + this.clientWidth;
                    var bottom = top + this.clientHeight;

                    // Is this the first element?
                    if (index == 0)
                        var first = true;

                    // Get info about the next element's position (if any)
                    try {
                        var next = $listItems[index+1];
                        var nextLeft = $(next).offset().left;
                        var nextTop = $(next).offset().top;
                        var nextBottom = $(next).offset().top + next.clientHeight;
                    }
                    catch (exception) {
                        var last = true;
                    }

                    /* case1: clicked before the first element
                     * case2: clicked after the last element
                     * case3: clicked between the end element on a row, and the next 
                     *          on the next row
                     * case4: clicked between two elements
                     */
                    if (first && eX < left && eY < bottom && eY > top) { 
                        $(this).before(plugin.$inputItem);
                        plugin.$inputField.focus();
                        return false;
                    }
                    else if(last) {
                        $(this).after(plugin.$inputItem);
                        plugin.$inputField.focus();
                    }
                    else if (eX > right && eY > top && eY < bottom && eX < nextLeft) {
                        $(this).after(plugin.$inputItem);
                        plugin.$inputField.focus();
                        return false;
                    }
                    else if (eX > right && eY < nextTop) {
                        $(this).after(plugin.$inputItem);
                        plugin.$inputField.focus();
                        return false;
                    }
                    else if (eX < left && eX < nextLeft && eY > nextTop && eY < nextBottom) {
                        $(this).after(plugin.$inputItem);
                        plugin.$inputField.focus();
                        return false;
                    }

                });
            }
        
        },

        /**
         * Namespace containing functions for handling events on the input.
         */
        _inputHandle: {

            focus: function(e) {
                if (this.$inputField.val().length > 0)
                    this._updateDropdown();
                else
                    this._resetDropdown();
            },

            blur: function(e) {
                this._resetDropdown();
            },
            
            keyup: function(e) {
                e.stopPropagation();
                
                var inputVal = this.$inputField.val().trim();
                var valLen = inputVal.length;
                 
                switch(e.keyCode) {
                    case 27: // esc key
                        if (this.$inputItem.data("name")) {
                            this._createListItemBefore(this._getDropdownData());
                            this.$inputItem.removeData();
                        }
                        else {
                            this._clearInput();
                            this._resetDropdown();
                        }
                        break;

                    case 13: // enter key
                        if (this._getDropdownData() && valLen > 0)
                            this._createListItemBefore(this._getDropdownData());
                        else {
                            this._resetDropdown();
                            this._clearInput();
                        }
                        break;
                    
                    case 8:  // backspace key
                        if (valLen == 0) {
                            this._resetDropdown();
                            this._clearInput();
                        }
                    case 37: // left arrow key
                        if (this.$inputField._status.cursorAtBeg) {
                            var $pe = this.$inputItem.prev();
                            if (this._getDropdownData())
                                this._createListItemAfter(this._getDropdownData());
                            if ($pe.length != 0)
                                this._editListItem($pe[0]);
                        }
                        break;

                    case 38: // up arrow key
                        this._prevDropdownItem();
                        break;

                    case 46: // delete key
                        if (valLen == 0) {
                            this._resetDropdown();
                            this._clearInput();
                        }
                    case 39: // right arrow key
                        if(this.$inputField._status.cursorAtEnd) {
                            var $ne = this.$inputItem.next();
                            if (this._getDropdownData())
                                this._createListItemBefore(this._getDropdownData());
                            if($ne.length != 0)
                                this._editListItem($ne[0]);
                        }
                        break;

                    case 40: // down arrow key
                        this._nextDropdownItem();
                        break;

                    default:
                        if (this.$inputField.val().length > 0)
                            this._updateDropdown();
                        else
                            this._resetDropdown();
                        break;
                }

                // always adjust the input width
                this._setInputWidth();
            },

            keydown: function(e) {
                e.stopPropagation();
                
                // position of the selection start and end
                var ss = e.target.selectionStart;
                var se = e.target.selectionEnd;

                var inputVal = this.$inputField.val();
                var valLen = inputVal.length;
                 
                switch(e.keyCode) {
                    case 8:  // backspace key
                    case 37: // left arrow key
                        // Just set the status in this.$inputField._status to be referred 
                        // to during the keyup event.
                        if (ss == 0 && se == 0)
                            this.$inputField._status.cursorAtBeg = true;
                        else
                            this.$inputField._status.cursorAtBeg = false;

                        break;

                    case 39: // right arrow key
                        
                        if (ss == valLen && se == valLen)
                            this.$inputField._status.cursorAtEnd = true;
                        else
                            this.$inputField._status.cursorAtEnd = false;
                        
                        break;

                    case 13: // enter key
                        e.preventDefault();
                        break;

                    default:
                        break;
                }
            },

            click: function(e) {
                e.stopPropagation();
            }

        },

        /**
         * Namespace containing functions for handling events on the input list item.
         */
        _listItemHandle: {

            mousedown: function(e) {
                e.preventDefault();
                e.stopPropagation();
            },

            click: function(e) {
                e.preventDefault();
                e.stopPropagation();
                this._editListItem(e.target);
            }

        },

        /**
         * Selects all chars in the input field.
         */
        _selectInput: function() {
            this.$inputField.select();
        },

        /**
         * Sets the current value of the input.
         * @param value     The value to set the input to.
         */
        _setInputVal: function(value) {
            this.$inputField.val(value);
        },

        /**
         * Clears out the list's $inputField and data.
         */
        _clearInput: function() {
            this.$inputField.val("");
            this.$inputItem.removeData();
        },

        /**
         * Sets the width of the input element in question.
         */
        _setInputWidth: function () {
            // use actual value of $inputField instead of trimmed value
            // so that users' spaces at beginning or end are there when they're typing
            // replace the spaces with &nbsp; due to HTML whitespace collapsing
            var $tempSpan = $('<span class="input-temp">' + 
                this.$inputField.val().replace(/\s/g, "&nbsp;") + 
                '</span>');
            $(document).find("body").append($tempSpan);
            var inputLength = $tempSpan.css("width");
            this.$inputField.css("width", inputLength);
            $tempSpan.remove();
        },

        /***********************************
         * Drowdown-related functions
         ***********************************/

        /**
         * Looks for matches in this.autocompleteData
         * @param str   The string to search on.
         * @return      An array of objects describing the match
         */
        _getMatches: function(str) {
            var matches = [];
       
            var self = this;
            this.autocompleteData.forEach(function(ce, index) {
                if (typeof self.options.dataTypesAccepted === 'undefined' || typeof ce.type !== 'undefined' && $.inArray(ce.type, self.options.dataTypesAccepted) != -1) {
                    var matchIndex = ce.name.toLowerCase().indexOf(str.toLowerCase());
                    if (matchIndex != -1) {
                        var obj = $.extend({matchIndex:matchIndex,matchLength:str.length}, ce);
                        matches.push(obj);
                    }
                }
            });
        
            return matches;
        },

        /**
         * Updates the dropdown autocomplete list with new matches.
         */
        _updateDropdown: function () {
            var self = this;
            var dd = this.$dropdown;
            
            dd.html('');
            
            var inputVal = this.$inputField.val().trim();
            var $inputItem = this.$inputItem;
            
            // add list item for literal
            if (typeof self.options.dataTypesAccepted === 'undefined' || $.inArray("literal", self.options.dataTypesAccepted) != -1) {
                var $literalLi = $('<li class="dropdown selected">' + inputVal + ' (literal)</li>');
                $literalLi.data({name:inputVal, type:"literal"});
                dd.append($literalLi);
            }

            this._getMatches(this.$inputField.val()).forEach(function(match) {
                var $li = $('<li class="dropdown">' + self.options.dropdownLiFormat(match) + '</li>');
                
                // clear out index info
                delete match.matchIndex;
                delete match.matchLength;
                $li.data(match);

                // check to see if this data matches up with existing element data
                var inputData = $inputItem.data();
                if (_.isEqual(inputData, match)) {
                    dd.find(".selected").removeClass("selected");
                    $li.addClass("selected");
                }

                dd.append($li);
            });

            // add click handlers to individual list items
            var listItems = dd.find("li");
            // This needs to be mousedown. Otherwise, the input blur fires first, and kills this event.
            listItems.on("mousedown", $.proxy(this._dropdownItemHandler.mousedown, this));
            listItems.on("click", $.proxy(this._dropdownItemHandler.click, this));
            listItems.on("mouseover", $.proxy(this._dropdownItemHandler.mouseover, this));
                
            dd.show();
        },

        /**
         * Gets the data values associated with the selected dropdown item.
         */
        _getDropdownData: function () {
            try {
                return this.$dropdown.find(".selected").data();
            }
            catch (ex) {
                return false;
            }
        },

        /**
         * Advances selection to the next dropdown item, if any.
         */
        _nextDropdownItem: function() {
            var $selected = this.$dropdown.find(".selected");
            var $next = $selected.next();
        
            if ($selected.length == 0) {
                this.$dropdown.find(".dropdown:first").addClass("selected");
            }
            else if ($next.length == 0) {
                $selected.removeClass("selected");
                this.$dropdown.find(".dropdown:first").addClass("selected");
            }
            else {
                $selected.removeClass("selected");
                $next.addClass("selected");
            }
        },

        /**
         * Advances selection to the previous dropdown item, if any.
         */
        _prevDropdownItem: function() {
            var $selected = this.$dropdown.find(".selected");
            var $prev = $selected.prev();

            if($selected.length == 0) {
                this.$dropdown.find(".dropdown:last").addClass("selected");
            }
            else if ($prev.length == 0) {
                $selected.removeClass("selected");
                this.$dropdown.find(".dropdown:last").addClass("selected");
            }
            else {
                $selected.removeClass("selected");
                $prev.addClass("selected");
            }
        },

        /**
         * Blanks out the dropdown.
         */
        _resetDropdown: function() {
            this.$dropdown.hide();
            this.$dropdown.html("");
        },

        /**
         * Namespace for events on the dropdown items.
         */
        _dropdownItemHandler: {

            mouseover: function(e) {
                this.$dropdown.find("li.dropdown.selected").removeClass("selected");
                $(e.currentTarget).addClass("selected");
            },
            
            mousedown: function(e) {
                e.preventDefault();
                e.stopPropagation();
            },

            click: function(e) {
                e.preventDefault();
                e.stopPropagation();
                this._createListItemBefore(this._getDropdownData());
            }
        }

    });

})(jQuery, window, document);
